export PATH=~/bin:/usr/local/bin:$PATH
export LC_ALL=en_US.UTF-8  
export LANG=en_US.UTF-8
####################################
#                ZSH               #
####################################
autoload -Uz compinit
compinit
####################################
#           History                #
####################################
HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=5000
bindkey -e
setopt share_history
setopt histignoredups
setopt histignorespace
setopt inc_append_history
setopt hist_expire_dups_first
setopt hist_ignore_all_dups
####################################
#           Prompt                 #
####################################
setopt prompt_subst
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
zstyle ':vcs_info:git:*' formats '%b'
zstyle ':vcs_info:*' enable git
PROMPT='%2~ $vcs_info_msg_0_%% '
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced
if uname | grep -q 'Linux' ; then
  alias ls='ls --color'
fi
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'
####################################
#              TMUX                #
####################################
alias tmux="tmux -2" # Use 256 colors in tmux
####################################
#           HOMEBREW               #
####################################
if type brew &>/dev/null; then
    HOMEBREW_PREFIX=$(brew --prefix)
    FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH
fi
####################################
#            React                #
####################################
export REACT_EDITOR='code'
####################################
#             Python               #
####################################
alias remove_pyc="find . -name \*.pyc -delete"
if [ -d '/Library/Python' ]; then
    export PATH=~/Library/Python/3.7/bin/:$PATH # pip3.7 install --user ...
fi
if command -v pyenv 1>/dev/null 2>&1;
    then eval "$(pyenv init -)"
fi
PATH=$HOME/.poetry/bin:$PATH
####################################
#             Node                 #
####################################
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
####################################
#             GCloud               #
####################################
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/j.poissonnier/Projects/google-cloud-sdk/path.zsh.inc' ]; then source '/Users/j.poissonnier/Projects/google-cloud-sdk/path.zsh.inc'; fi
# The next line enables shell command completion for gcloud.
if [ -f '/Users/j.poissonnier/Projects/google-cloud-sdk/completion.zsh.inc' ]; then source '/Users/j.poissonnier/Projects/google-cloud-sdk/completion.zsh.inc'; fi
####################################
#                Go                #
####################################
if [ -d $HOME'/Projects' ]; then
    export GOPATH=~/Projects/go
fi
export PATH="$(go env GOPATH)/bin:$PATH"
####################################
#              Ripgrep             #
####################################
export RIPGREP_CONFIG_PATH=$HOME/.ripgreprc
####################################
#            Kubernetes            #
####################################
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
export PATH="/usr/local/opt/helm@2/bin:$PATH"
####################################
#                FZF               #
####################################
export FZF_DEFAULT_COMMAND='rg --files --hidden --follow --glob "!.git/*" --ignore-file ~/.gitignore_global'
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
